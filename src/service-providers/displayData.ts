import { Injectable } from "@angular/core";
import { HttpClient, HttpParams } from "@angular/common/http";
import { CommonServiceProvider } from "./common-service";



@Injectable()
export class DisplayDataProvider{

    constructor(
        public http: HttpClient, 
        private services: CommonServiceProvider
        )
         {
            console.log('Hello Display Data Provider');
        }
        saveDisplay(params) {
            return this.http.post(
                this.services.getServices("saveDisplay"), 
                params,);
      }
        getAllDisplay() {
            return this.http.get(
                this.services.getServices("getAllDisplay"), );
        }

        deleteDisplay(id: any) {
            return this.http.delete("http://localhost:8080/DisplayHibernate/display/deleteDisplay/"+id,
            /*{
              params: new HttpParams().set('id', id),
            }*/);
          }
        updateDisplay(id: any,params) {
        return this.http.put("http://localhost:8080/DisplayHibernate/display/updateDisplay/"+id,
                params);
        }
        saveImage(params) {
            return this.http.post(
                this.services.getServices("saveImage"), 
                params,);
      }
}