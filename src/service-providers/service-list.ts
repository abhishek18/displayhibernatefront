import { Injectable } from "@angular/core";


@Injectable()
export class ServiceList{
    public explore(){
        return{
            saveDisplay : "display/saveDisplay",
            getAllDisplay: "display/getAllDisplay",
            deleteDisplay: "display/deleteDisplay",
            saveImage: "image/saveImage",
        }
    }
}