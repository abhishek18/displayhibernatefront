import { Timestamp } from "rxjs";

export interface DisplayInfo{
        displayId?:string,
        displayText?:string,
        textInfo?:string,
        firstName?:string,
        lastName?:string,
        email?:string,
        createdBy?:string,
        createdOn?:Date,
        modifiedBy?:string,
        modifiedOn?:Date,
        defunct?:string,
        bold?:string,
        italic?:string,
        underline?:string,
}

export interface ResponseData{
    responseStatus?:string,
    responseCode?:string,
    responseObj?:string,
    responseUidUserType?:string,
    responseMsg?:string,
    obj?: any
}

export interface ImageInfo{
    imgId?: number,
    imgTxt?:string,
    imgFile?:any,
    imgInfo?: string,
    createdBy?:string,
    createdOn?:Date,
    modifiedBy?:string,
    modifiedOn?:Date,
    defunct?:string,
    flag?: string
}