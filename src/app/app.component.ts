import { Component } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'DisplayHibernateFront';

  constructor(
    private httpClient: HttpClient,
    private router: Router,
  ){
    
  }

  goDisplayPage(event){

    console.log('You clicked to go Display>> ');
    this.router.navigate(['display']);
  }
}
