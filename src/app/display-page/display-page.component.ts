import { Component, OnInit, AfterViewInit, ViewChild, ElementRef, ViewEncapsulation } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { DisplayInfo, ResponseData, ImageInfo } from '../interface';
import { DisplayDataProvider } from 'src/service-providers/displayData';
import { JavaService } from 'src/service-providers/service.status';
//import { ToastrManager } from 'ng6-toastr-notifications';
import * as MyScriptJS from 'myscript';
//import { Server } from 'net';

@Component({
  encapsulation: ViewEncapsulation.None,
  selector: 'app-display-page',
  templateUrl: './display-page.component.html',
  styleUrls: ['./display-page.component.css']
})
export class DisplayPageComponent implements OnInit  {

  @ViewChild("tref", {read: ElementRef}) domEditor: ElementRef;

  public expres:string="";
  model: any={};
  displayList: Array<DisplayInfo> = [{}];
  showSubmit: boolean = true;
  imageObj: any={};
  editor;
  file: any;
  constructor(
    private httpClient: HttpClient,
    private displayService: DisplayDataProvider,
    private service: JavaService,
    //public toastr: ToastrManager,
  ){
   
  }

  ngOnInit() {
  }

  getText(inpt){
    console.log("from html >> ",inpt);
    switch (inpt) {
      case 'txt': 
        this.expres = "T" ;
        this.getTextData();       
        break;
      case 'img': 
          this.expres = "I"        
        break;
      case 'vdo': 
        this.expres = "V"
        break;
    
      default:
        break;
    }
  }
  textSubmit() {
    console.log('SUCCESS!! :-)\n\n' + JSON.stringify(this.model));

    return new Promise((resolve, reject) => {
      let dispObj: DisplayInfo = {};
      dispObj={
        "displayId":"",
        "displayText":"Y",
        "textInfo":this.model.subject,
        "firstName":this.model.firstName,
        "lastName":this.model.lastName,
        "email":this.model.email,
        "bold":this.model.bold,
        "italic":this.model.italic,
        "underline":this.model.underline,
        "createdBy":"Abhis",
        "createdOn": new Date(),
        
        "defunct":"N"
      }
      this.displayService.saveDisplay(dispObj).subscribe((success: any) => {
        this.service.status(success).catch(err => {
          if(err) {
           console.log("error in save >> ");
            //this.alert.presentAlert(err.messageSeverity, err.message, 'OK')
          }
        }).then((data: ResponseData) => {
          if (data.responseStatus == "success") {           
            alert(data.responseMsg); 
            this.model.subject = "";
            this.showSubmit = true;
            this.getTextData();         
           } else {
            console.log("Oops!! Data save failed");
          } 
        })
      }, error => {
        console.log("error in Big save >> ");
      });
    });
  }
  getTextData(){
    console.log('inside get\n\n' + JSON.stringify(this.model));
    return new Promise((resolve, reject) => {
      
      this.displayService.getAllDisplay().subscribe((success: any) => {
        this.service.status(success).catch(err => {
          if(err) {
           console.log("error in getText >> ");
            //this.alert.presentAlert(err.messageSeverity, err.message, 'OK')
          }
        }).then((data: ResponseData) => {
          if (data.responseStatus == "success") { 

            console.log("From DB >> ",JSON.stringify(data.obj));          
            this.displayList = data.obj;    
            //data.obj.forEach((element)=>{});
          } else {
            console.log("Oops!! Data fetch failed");
          } 
        })
      }, error => {
        console.log("error in Big fetch >> ");
      });
    });
  }
  //--------Delete display Object By Id----------//
  deleteDisplay(delId){
    console.log("Id to delete >> "+delId);

    return new Promise((resolve, reject) => {      
      this.displayService.deleteDisplay(delId).subscribe((success: any) => {
        this.service.status(success).catch(err => {
          if(err) {
           console.log("error in delete >> ");
            //this.alert.presentAlert(err.messageSeverity, err.message, 'OK')
          }
        }).then((data: ResponseData) => {
          if (data.responseStatus == "success") { 

            console.log("Successfully deleted >> ",data.responseMsg);          
            alert(data.responseMsg); 
            this.getTextData();      
          } else {
            console.log("Oops!! Data save failed");
          } 
        })
      }, error => {
        console.log("error in Big save >> ");
      });
    });
  }
  //--------End Of Delete display Object By Id----------//
  editDisplay(updObj){
    console.log("Id to update >> "+JSON.stringify(updObj));

    this.model.subject = updObj.textInfo;
    this.model.displayId = updObj.displayId;
    this.model.createdOn = updObj.createdOn;
    this.model.createdBy = updObj.createdBy;
    this.model.modifiedBy = "Abhis";
    this.model.modifiedOn = new Date();
    this.showSubmit = false;
    this.model.bold =  updObj.bold;
    this.model.italic =  updObj.italic;
    this.model.underline =  updObj.underline;

    if(this.model.bold == 'Y') {
      this.setFont('B');

    } else if(this.model.italic == 'Y') {
      this.setFont('I');

    } else if(this.model.underline == 'Y') {
      this.setFont('U');
    }
  }
  updateDisplay(updateObj){
    console.log("updateObj to update >> "+JSON.stringify(updateObj));

    return new Promise((resolve, reject) => {
      let dispObj: DisplayInfo = {};
      dispObj={
        "displayId":updateObj.displayId,
        "displayText":"Y",
        "textInfo":updateObj.subject,        
        "createdBy":updateObj.createdBy,
        "createdOn": updateObj.createdOn,
        "modifiedBy":updateObj.modifiedBy,
        "modifiedOn": updateObj.modifiedOn,
        "defunct":"N",
        "bold":updateObj.bold,
        "italic":updateObj.italic,
        "underline":updateObj.underline,
      }
      this.displayService.updateDisplay(updateObj.displayId,dispObj).subscribe((success: any) => {
        this.service.status(success).catch(err => {
          if(err) {
           console.log("error in update >> ");
            //this.alert.presentAlert(err.messageSeverity, err.message, 'OK')
          }
        }).then((data: ResponseData) => {
          if (data.responseStatus == "success") {           
            alert(data.responseMsg); 
            this.model.subject = "";
            this.showSubmit = true;
            this.getTextData();         
           } else {
            console.log("Oops!! Data update failed");
          } 
        })
      }, error => {
        console.log("error in Big update >> ");
      });
    });
  }

  setFont(type){
    console.log("inside SetFont >> ",type);
      
      switch (type) {
        case "I":
          document.getElementById("subject").style.textDecoration  = ""; 
          document.getElementById("subject").style.font = "italic 16px arial,serif";
          this.model.bold = "N"
          this.model.italic = "Y"
          this.model.underline = "N"
          break;

        case "B":
          document.getElementById("subject").style.textDecoration  = ""; 
          document.getElementById("subject").style.font = "bold 17px arial,serif";
          this.model.bold = "Y"
          this.model.italic = "N"
          this.model.underline = "N"
          break;

        case "U":
          document.getElementById("subject").style.textDecoration  = "underline";  
          document.getElementById("subject").style.font = "16px arial,serif";
          this.model.bold = "N"
          this.model.italic = "N"
          this.model.underline = "Y"
          break;
      
        default: console.log("Sorry >> ",type);
          break;
      }
  }

  onFileChanged(event) {
    this.file = event.target.files[0];
    console.log("File >> ",this.file);

  }
  imageSubmit(){

    return new Promise((resolve, reject) => {
      let imgSaveObj: ImageInfo = {};
      imgSaveObj={
        
        "imgTxt":"Y",
        "imgFile":this.file,
        "imgInfo":this.imageObj.imgInfo,
        "createdBy":"Abhis",
        "createdOn": new Date(),
        "defunct":"N",
        "flag":"I"
      }
      this.displayService.saveImage(imgSaveObj).subscribe((success: any) => {
        this.service.status(success).catch(err => {
          if(err) {
           console.log("error in save >> ");
            //this.alert.presentAlert(err.messageSeverity, err.message, 'OK')
          }
        }).then((data: ResponseData) => {
          if (data.responseStatus == "success") {           
            alert(data.responseMsg); 
            this.imageObj.imgInfo = "";
            
            //this.getTextData();         
           } else {
            console.log("Oops!! Data save failed");
          } 
        })
      }, error => {
        console.log("error in Big save >> ");
      });
    });
  }

  /*Success(title:string,message?:string){
    this.toastr.successToastr(title,message);
  }
  Warning(title:string,message?:string){
    this.toastr.warningToastr(message,title);
  }
  Error(message:string,title?:string){
    this.toastr.errorToastr(message,title);
  }
  Info(message:string){
    this.toastr.infoToastr(message);
  }*/
}
