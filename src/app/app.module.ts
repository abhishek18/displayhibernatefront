import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DisplayPageComponent } from './display-page/display-page.component';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { JavaService } from 'src/service-providers/service.status';
import { DisplayDataProvider } from 'src/service-providers/displayData';
import { CommonServiceProvider } from 'src/service-providers/common-service';
import { ServiceList } from 'src/service-providers/service-list';

@NgModule({
  declarations: [
    AppComponent,
    DisplayPageComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    FormsModule
  ],
  providers: [
    JavaService,
    DisplayDataProvider,
    CommonServiceProvider,
    ServiceList,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
